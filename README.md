# legacytranslate

Internationalization library of legacylisten.

The internationalization module of legacylisten was extracted and
made to it's own crate – this one – to improve readability and
reusability.

`L10n` is the central type of this crate, it's supposed to hold
all language information of the program (although you can have
multiple `L10n`s if you e.g. want multiple languages).  It's
[`write`](L10n::write) method prints the message given to it.
Messages must implement the `Message` trait which converts an
easy to use type to all the data that are needed under the hood.

`L10n`s are constructed out of a value that implements the
`Lang` trait.

See [`legacylisten`](https://crates.io/crates/legacylisten) to see
how this crate is best used.

### Contributing
As every software `legacytranslate` too always can be improved.
While I'm trying to get it usable alone, I don't have unlimited
time and especially not always the best ideas.  If you can help
with that or on some other way (like with a feature request or
documentation improvements) **please help**.

I assume that unless stated otherwise every contribution follows the
necessary license.

### License
Though unusual for a rust program, `legacytranslate` is released
under the GNU General Public License version 3 or (at your option)
any later version.

For more see
[LICENSE.md](https://codeberg.org/zvavybir/legacytranslate/src/branch/master/LICENSE.md).
